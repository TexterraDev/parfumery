$(document).ready(function() {

    $('.mob__menu__btn').click(function() {

		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
			$('.main__menu').removeClass('active');
		} else {
			$(this).addClass('active');
			$('.main__menu').addClass('active');
		}
	});

	$(window).bind('scroll.once', function () {
      if ($(window).scrollTop() > 60) {
  		  $('.mob__menu__btn').addClass('positioned');
      } else {
  		  $('.mob__menu__btn').removeClass('positioned');
      }
  	});

	function stockSlider() {
		if ($(window).width() < 1024) {
			if(!$('.stock__block__products__wrap').hasClass('slick-initialized')) {
				$('.stock__block__products__wrap').slick({
				  slidesToShow: 3,
				  slidesToScroll: 1,
				  arrows: true,
				  rows: 1,
				  infinite: false,
				  adaptiveHeight: true,
				  responsive: [
				    {
				      breakpoint: 768,
				      settings: {
				        slidesToShow: 2,
				        slidesToScroll: 1
				      }
				    },
				    {
				      breakpoint: 576,
				      settings: {
				        slidesToShow: 1,
				        slidesToScroll: 1
				      }
				    }
				  ]
				});

				$(window).resize(function() {
					$(".stock__block__products__wrap")[0].slick.refresh();
				});
			};

		} else {
			if($('.stock__block__products__wrap').hasClass('slick-initialized')) {
	            $('.stock__block__products__wrap').slick("unslick");
	        }
		}
	};

	stockSlider();

	$(window).resize(function() {
		stockSlider();
	});

	function reviewActive() {
		if ($('.reviews__nav__slider__item.active').length) {
			var slideData = $(this).data('slide');

			$(".reviews__main__slider").slick("slickUnfilter");
			$(".reviews__main__slider").slick("slickFilter", '[data-slide="' + slideData + '"]');
			$(".reviews__main__slider")[0].slick.refresh();
		}
	};

	function screenActive() {
		var screenActive = $('.screen__block.active').data('screen');

		$('.main__menu__item .main__menu__item__current').not('[data-screen="'+screenActive+'"]').parents('.main__menu__item').removeClass('active');
		$('.main__menu__item .main__menu__item__current[data-screen="'+screenActive+'"]').parents('.main__menu__item').addClass('active');
	};

	screenActive();

	function productActive() {
		if ($('.stock__block__product.active').length) {
			var productData = $('.stock__block__product.active').data('product');
			var productTitle = $('.stock__block__product.active').children('.stock__block__product__title').text();
			var productText = $('.stock__block__product.active').children('.stock__block__product__text').text();
			var productPrice = $('.stock__block__product.active').children('.stock__block__product__price').text();

		  	$('.stock__block__products__info__title').text(productTitle);
			$('.stock__block__products__info__text').text(productText);
			$('.stock__block__products__info__price').text(productPrice);
		}
	}

	productActive();

	function sideNav() {
		var screenData = $('.screen__block.active').data('screen');
		var screenTitle = $('.screen__block.active').data('title');

		if ($('.screen__block.active').prev().length) {
			var screenPrevData = $('.screen__block.active').prev().data('screen');
			var screenPrevTitle = $('.screen__block.active').prev().data('title');

			$('.side__nav__prev').addClass('hidden');
			setTimeout(function() {
				$('.side__nav__prev').data('screen', screenPrevData);
				$('.side__nav__prev .side__nav__text').text(screenPrevTitle);
				$('.side__nav__prev').removeClass('hidden');
			}, 280);
		} else {
			$('.side__nav__prev').addClass('hidden');
		}

		if ($('.screen__block.active').next().length) {
			var screenNextData = $('.screen__block.active').next().data('screen');
			var screenNextTitle = $('.screen__block.active').next().data('title');

			$('.side__nav__next').addClass('hidden');
			setTimeout(function() {
				$('.side__nav__next').data('screen', screenNextData);
				$('.side__nav__next .side__nav__text').text(screenNextTitle);
				$('.side__nav__next').removeClass('hidden');
			}, 280);
		} else {
			$('.side__nav__next').addClass('hidden');
		}

		if ($(window).width() > 1023) {
			if ($('.services__block.active').length) {
				$('.btn__partners').addClass('hidden');
			} else {
				$('.btn__partners').removeClass('hidden');
			}
		}
	};

	sideNav();

	$('.reviews__nav__slider').slick({
	  slidesToShow: 5,
	  slidesToScroll: 1,
	  arrows: true,
	  vertical: true
	});

	$('.reviews__main__slider').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: true,
	  adaptiveHeight: true
	});

	$('.stock__block__product').not('.coming-soon').children('.stock__block__product__image, .stock__block__product__title').click(function(){

		if ($(window).width() < 1024) {

			var productData = $(this).parents('.stock__block__product').data('product');
			var productTitle = $(this).parents('.stock__block__product').children('.stock__block__product__title').text();
			var productText = $(this).parents('.stock__block__product').children('.stock__block__product__text').text();
			var productPrice = $(this).parents('.stock__block__product').children('.stock__block__product__price').text();

			$('.overlay').fadeIn();
			$('.stock__block__products__info.popup').addClass('active');
		  	$('.stock__block__products__info__title').text(productTitle);
			$('.stock__block__products__info__text').text(productText);
			$('.stock__block__products__info__price').text(productPrice);
		}
	});

	$('.side__nav__prev, .side__nav__next').click(function() {

		var screenData = $(this).data('screen');

		$('.screen__block.active').removeClass('active');
		$('.screen__block[data-screen="'+screenData+'"]').addClass('active');

		$('.main__menu__item.active').removeClass('active');
		$('.main__menu__item .main__menu__item__current[data-screen="'+screenData+'"]').parents('.main__menu__item').addClass('active');

		sideNav();
	});

	$('.main__menu__item__current').click(function() {
		if ($(this).hasClass('active')) {

		} else {
			var screenData = $(this).data('screen');

			$(this).parents('.main__menu__item').addClass('active');
			$(this).parents('.main__menu__item').siblings('.main__menu__item').removeClass('active');

			$('.screen__block.active').removeClass('active');
			$('.screen__block[data-screen="'+screenData+'"]').addClass('active');

			sideNav();
		}

		if ($(window).width() < 1024) {
			var screenData = $(this).data('screen');

			$('html, body').animate({
			      scrollTop: $('[data-screen="'+screenData+'"]').offset().top - 10 
			  }, 500);

			$('.mob__menu__btn').removeClass('active');
			$('.main__menu').removeClass('active');
		}
	});

	$('.main__menu__item__prev').click(function(){
		var screenData = $(this).data('screen');

		$(this).parents('.main__menu__item').removeClass('active');
		$(this).parents('.main__menu__item').siblings('.main__menu__item').removeClass('active');
		$(this).parents('.main__menu__item').prev('.main__menu__item').addClass('active');

		$('.screen__block.active').removeClass('active');
		$('.screen__block[data-screen="'+screenData+'"]').addClass('active');

		sideNav();
	});

	$('.main__menu__item__next').click(function(){
		var screenData = $(this).data('screen');

		$(this).parents('.main__menu__item').removeClass('active');
		$(this).parents('.main__menu__item').siblings('.main__menu__item').removeClass('active');
		$(this).parents('.main__menu__item').next('.main__menu__item').addClass('active');

		$('.screen__block.active').removeClass('active');
		$('.screen__block[data-screen="'+screenData+'"]').addClass('active');

		sideNav();
	});

	// $('.main__block__item__nav__next').click(function(){
		// $(this).parents('.main__block__item').removeClass('fade-bottom fade-top active');
		// $(this).parents('.main__block__item').next('.main__block__item').removeClass('fade-bottom').addClass('active fade-top');
	// });

	// $('.main__block__item__nav__prev').click(function(){
	// 	$(this).parents('.main__block__item').removeClass('fade-bottom fade-top active');
	// 	$(this).parents('.main__block__item').prev('.main__block__item').removeClass('fade-top').addClass('active fade-bottom');
	// });

	$('.main__block__item').click(function(){

		if (!$(this).hasClass('active')) {
			if ($(this).prevAll('.active').length) {
				$(this).siblings().removeClass('active fade-bottom fade-top');
				$(this).addClass('active fade-top');
			} else {
				$(this).siblings().removeClass('active fade-bottom fade-top');
				$(this).addClass('active fade-bottom');
			}
		}
	});

	$('.advantages__block__list__item__title').click(function(){
		if ($(this).parents('.advantages__block__list__item').hasClass('active')) {
			$(this).parents('.advantages__block__list__item').removeClass('active');
			$(this).siblings('.advantages__block__list__item__content').slideUp();
		} else {
			$(this).parents('.advantages__block__list__item').siblings().removeClass('active');
			$(this).parents('.advantages__block__list__item').siblings().children('.advantages__block__list__item__content').slideUp();
			$(this).parents('.advantages__block__list__item').addClass('active');
			$(this).siblings('.advantages__block__list__item__content').slideDown();
		}
	});

	if ($('.advantages__block__list__item.active').length) {
		$('.advantages__block__list__item.active').children('.advantages__block__list__item__content').slideDown();
	};

	$('.reviews__nav__slider__item').click(function() {
		var dataSlide = $(this).data('slide');

		$(".reviews__main__slider").slick("slickUnfilter")
		$(".reviews__main__slider").slick("slickFilter", '[data-slide="' + dataSlide + '"]');
		$(".reviews__main__slider")[0].slick.refresh()

		if ($(this).hasClass('active')) {

		} else {
			$(this).addClass('active');
			$(this).siblings().removeClass('active');
		}

		if ($(window).width() < 1024) {
			$('html, body').animate({
			      scrollTop: $('.reviews__main__slider').offset().top - 10 
			  }, 500);
		}

		$(".reviews__main__slider__item").overlayScrollbars({ });
	});

	if ($('.reviews__nav__slider__item.active').length) {
		var dataSlide = $('.reviews__nav__slider__item.active').data('slide');

		$(".reviews__main__slider").slick("slickUnfilter");
		$(".reviews__main__slider").slick("slickFilter", '[data-slide="' + dataSlide + '"]');
		$(".reviews__main__slider")[0].slick.refresh();
	};
		

	$('.overlay').click(function() {
		$(this).fadeOut();
		$('.popup').removeClass('active');
	});


	// $('.present__video__popup').click(function() {
	// 	$('.overlay').fadeOut();
	// 	$(this).removeClass('active');
	// });

	$('.present__video__preview').click(function() {
	  $(this).addClass('hidden');
	  $(".present__video__popup iframe").attr('src', $(".present__video__popup iframe").attr('src') + '?autoplay=1'); 
	});

	$('.popup__close').click(function() {
		$('.overlay').fadeOut();
		$('.popup').removeClass('active');
	});

	$('.get__present__video').click(function(event) {
		event.preventDefault();
		$('.overlay').fadeIn();
		$('.present__video__popup').addClass('active');
	});

	$('.stock__block__product').not('.coming-soon').click(function(){
		var productData = $(this).data('product');
		var productTitle = $(this).children('.stock__block__product__title').text();
		var productText = $(this).children('.stock__block__product__text').text();
		var productPrice = $(this).children('.stock__block__product__price').text();

		if (!$(this).hasClass('active')) {
			$(this).addClass('active');
			$(this).siblings('.stock__block__product').removeClass('active');

			$('.stock__block__products__info').removeClass('slide-in').addClass('slide-out');

			setTimeout(function() {
			  	$('.stock__block__products__info__title').text(productTitle);
				$('.stock__block__products__info__text').text(productText);
				$('.stock__block__products__info__price').text(productPrice);
				$('.stock__block__products__info').removeClass('slide-out').addClass('slide-in');
			}, 350);
		}
	});

	// $('.stock__block__product').not('.coming-soon').mouseover(function() {
	// 	$(this).siblings('.stock__block__product').removeClass('active');
	// });

	$('.btn__partners').click(function(){
		$(this).addClass('clicked');

		setTimeout(function() {
			$('.btn__partners').removeClass('clicked');
		}, 450);

		if ($(window).width() < 1024) {

			$('html, body').animate({
			    scrollTop: $('.services__block').offset().top - 10 
			}, 500);

		} else {

			$('.screen__block').not('[data-screen="7"]').removeClass('active');
			$('.services__block').addClass('active');

			$('.main__menu__item .main__menu__item__current').not('[data-screen="7"]').parents('.main__menu__item').removeClass('active');
			$('.main__menu__item .main__menu__item__current[data-screen="7"]').parents('.main__menu__item').addClass('active');
		};

		sideNav();
	});

	$(".reviews__main__slider__item").overlayScrollbars({ });

	$('.input__phone').inputmask({
        "mask": "8 (999) 999-99-99",
        "clearIncomplete": true,
        "onincomplete": function(){$(this).parents('form').find('button').attr("disabled", true), $(this).parents('label').addClass('input-error')},
        "oncomplete": function(){$(this).parents('form').find('button').attr("disabled", false), $(this).parents('label').removeClass('input-error')}
    });

    $('body').on('mousewheel DOMMouseScroll', function(event){
    	var dataScreen = $('.screen__block.active').data('screen');

    	if ($(window).width() > 1023) {

	    	if (event.originalEvent.wheelDelta >= 0) {

	    		if ($('.screen__block[data-screen="'+dataScreen+'"]').prev('.screen__block').length) {
	    			$('.screen__block[data-screen="'+dataScreen+'"]').removeClass('active');
	    			$('.screen__block[data-screen="'+dataScreen+'"]').prev('.screen__block').addClass('active');
	    		}
	    	}

	    	else {

	    		if ($('.screen__block[data-screen="'+dataScreen+'"]').next('.screen__block').length) {
	    			$('.screen__block[data-screen="'+dataScreen+'"]').removeClass('active');
	    			$('.screen__block[data-screen="'+dataScreen+'"]').next('.screen__block').addClass('active');
	    		}
	    	}
	    };

    	sideNav();
    	screenActive();
	});
});