$(document).ready(function() {

	$('.leave__request__form').submit(function(e){
		e.preventDefault();

		let name = $(this).find('input[name="name"]');
		let phone = $(this).find('input[name="phone"]');
		let email = $(this).find('input[name="email"]');
		let city = $(this).find('input[name="city"]');

		if(name.val() == '') {name.parent().addClass('input__error')}else{name.parent().removeClass('input__error')}
		if(phone.val() == '') {phone.parent().addClass('input__error')}else{phone.parent().removeClass('input__error')}
		if(email.val() == '' || !isValidEmailAddress(email.val())) {email.parent().addClass('input__error')}else{email.parent().removeClass('input__error')}
		if(city.val() == '') {city.parent().addClass('input__error')}else{city.parent().removeClass('input__error')}

		if ($(this).find('.input__error').length == 0) {
			$('.leave__request__form__wrap').addClass('submited');
			yaCounter51354193.reachGoal('send_form_from_page_conditions');
			gtag('event', 'click', { 'event_category': 'clicks', 'event_label' : 'send_form_from_page_conditions'});
		}
	});
});

$('input, textarea').click(function () {
    if ($(this).parents('label').hasClass('input__error')) {
        $(this).parents('label').removeClass('input__error');
    }
});

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}